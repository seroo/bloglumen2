<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct(\App\Comment $comment)
    {
        $this->comment = $comment;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = $this->$comment->paginate(20);
        $data = $comments['data'];
        $response = [
            'data' => $data,
            'total_count' => $comments['total'],
            'limit' => $comments['per_page'],
            'pagination' => [
                'next_page' => $comments['next_page_url'],
                'current_page' => $comments['current_page']
            ]
        ];
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validationRules = [
            'comment' => 'required|min:1',
            'post_id' => 'required|exists:posts, id',
            'user_id' => 'required|exists:users, id'
        ];

        $validator = \Validator::make($input, $validationRules);
        if ($validator->fails()) {
            return new \Illuminate\Http\JsonResponse(
                [
                    'errors' => $validator->errors()
                ], \Illuminate\Http\Response::HTTP_BAD_REQUEST
            );
        }

        $comment = $this->comment->create($input);

        return ['data' => $comment];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = $this->comment - find($id);

        if (!$comment) {
            abort(404);
        }

        return $comment;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $comment = $this->comment->find($id);
        if (!$comment) {
            abort(404);
        }

        $comment->fill($input);
        $comment->save();

        return $comment;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = $this->comment->find($id);
        if (!$comment) {
            abort(404);

            return [
                'message' => 'deleted successfully',
                'comment_id' => $id
            ];
        }
    }
}
