<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [
    'middleware' => 'api.throttle',
    'limit' => 100,
    'expires' => 5,
    'prefix' => 'api/v1',
    'namespace' => "App\Http\Controllers\v1"
    ],
    function ($api) {
    $api->resource('posts', "PostController");
   $api->resource('comments', "CommentController", [
       'except' => ['store', 'index']
   ]) ;

   $api->post('posts/{id}/comments', 'CommentController@store');
   $api->get('posts/{id}/comments', 'CommentController@index');

});

$app->get('/', function () use ($app) {
    return $app->version();
});
